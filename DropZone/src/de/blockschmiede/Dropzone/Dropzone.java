package de.blockschmiede.Dropzone;

import java.util.logging.Logger;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import de.blockschmiede.util.FileUtil;


public class Dropzone extends JavaPlugin{

	Logger log = Logger.getLogger("Minecraft");
	
	public final FileUtil fileutil = new FileUtil(this);

	public PermissionManager permissionManager;
	
	
	@Override
	public void onDisable() {
		
		// TODO Auto-generated method stub
	}

	@Override
	public void onEnable() {
		// TODO Auto-generated method stub
		
		setupPermissions();
		
		this.saveDefaultConfig();
		
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		if(!(sender instanceof Player) || this.permissionManager.has((Player) sender, "dropzone.admin")){
			if (cmd.getLabel().equalsIgnoreCase("dropzone")||cmd.getLabel().equalsIgnoreCase("dz")){
				if(args.length>1 && args[0].equalsIgnoreCase("start")){
					DropzoneEvent event = new DropzoneEvent(args[1],this);
					if(event.exists()){
						event.setLootTable(args[1]);
						event.startEvent();
					}
				}
			}
		}
		return true;
	}
	
	private void setupPermissions() {			//Methode zur Verbindung mit dem Rechteverwaltungsplugin PermissionsEx

		if (permissionManager != null) {
			return;
		}

		Plugin permissionsPlugin = this.getServer().getPluginManager()
				.getPlugin("PermissionsEx");
		// Plugin permissionsPlugin =
		// this.getServer().getPluginManager().getPlugin("Permissions");

		if (permissionsPlugin == null) {
			log.info("Permission system not detected, defaulting to OP");
			return;
		}

		permissionManager = PermissionsEx.getPermissionManager();
		log.info("Found and will use plugin "
				+ ((PermissionsEx) permissionsPlugin).getDescription()
						.getFullName());

	}


}
