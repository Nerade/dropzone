package de.blockschmiede.Dropzone;

import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;


public class Drop {
	private int id,value,amount,limit;
	private ItemStack item;
	private String category,eventName;
	private double chance;
	private List<Location> spawns;
	private Dropzone plugin;
	
	
	public Drop(int id,int value,int amount,double chance,int limit,String category,List<Location>spawns,String eventName, Dropzone plugin){
		this.id=id;
		this.value=value;
		this.amount=amount;
		this.chance=chance;
		this.limit=limit;
		this.category=category;	
		this.spawns=spawns;
		this.eventName=eventName;
		MaterialData material = new MaterialData(id);
		material.setData((byte)value);
		item = material.toItemStack(amount);
		this.plugin=plugin;
	}
	public double getChance(){
		return chance;
	}
	public List<Location> getSpawns(){
		return spawns;
	}
	public int drop(World world,Location dropLocation){
		
			limit=limit-1;
			world.dropItem(dropLocation, item);	
			//plugin.fileutil.log(eventName,"Droppe "+id+":"+value+" Limit: "+limit);
			return amount;
	}
	public int getLimit(){
		return limit;
	}

	public String getCategory(){
		return category;
	}
	public int getId(){
		return id;
	}
	public int getValue(){
		return value;
	}
}
