package de.blockschmiede.Dropzone;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

public class DropzoneEvent {

	private String id, name;
	private Dropzone plugin;
	private int duration, droprate;
	private Date startTime;
	private String lootTable;
	private World world;
	private List<Drop> dropList;
	private HashMap<String,Location> dropLocations;
	private HashMap<String, Integer> dropCategories;

	DropzoneEvent(String id, Dropzone instance) {
		this.id = id;
		lootTable=id;
		this.plugin = instance;
		dropCategories = new HashMap<String, Integer>();
		dropLocations = new HashMap<String,Location>();
		dropList = new ArrayList<Drop>();

	}

	public int addLocation(String name, Location newLocation) {
		dropLocations.put(name,newLocation);
		return dropLocations.size();
	}

	public void setDuration(int minutes) {
		duration = minutes;
	}

	public boolean startEvent() {
		startTime = Calendar.getInstance().getTime();
		plugin.getServer().getScheduler()
				.scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						generateLoot();
					}
				}, droprate * 20);
		return true;
	}

	public void setLootTable(String lootTable) {
		this.lootTable = lootTable;
		readLootTable();
	}

	public boolean generateLoot() {
		boolean flag = false;
		plugin.fileutil.log(name,"Generiere Loot...");

		for (Drop tmpDrop : dropList) {

			if(tmpDrop.getLimit()>0){
				if(dropCategories.get(tmpDrop.getCategory())>0){
					if (calculateLoot(tmpDrop.getChance())) {
						tmpDrop.drop(world,
								calculateDropzone(tmpDrop.getSpawns()));
						registerLoot(tmpDrop.getCategory());
						plugin.fileutil.log(name,"Droppe "+tmpDrop.getId()+":"+tmpDrop.getValue()+" Limit: "+tmpDrop.getLimit());
						flag=true;
					}
				}
			}
		}
		if ((startTime.getTime() + duration * 60000) > System
				.currentTimeMillis()) {
			plugin.getServer().getScheduler()
					.scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							generateLoot();
						}
					}, droprate * 20);
		} else {
			plugin.fileutil.log(name,"Event beendet!");
		}
		return flag;
	}

	private Location calculateDropzone(List<Location> spawns) {
		int random = (int) (Math.random() * (spawns.size() - 1)+0.5);
		plugin.fileutil.log(name, "Berechne Spawn: "+random + " // " + spawns.size());
		return spawns.get(random);
	}

	private boolean calculateLoot(double chance) {
		int quantil = (int) (1000 * chance);
		int random = (int) (Math.random() * 1000);
		if (random >= quantil) {
			return false;
		} else {
			return true;
		}
	}
	
	public void registerLoot(String categoryName){
		
		dropCategories.put(categoryName,dropCategories.get(categoryName)-1);
		
	}
	public boolean exists(){
		return plugin.fileutil.eventExists(lootTable);
	}
	
	private void saveLootTable() {
		FileConfiguration config = plugin.fileutil.getCustomConfig(lootTable);
		
		config.set("dropevent.name",name);
		config.set("dropevent.world",this.world.getName());
		config.set("dropevent.duration", duration);
		config.set("dropevent.droprate", droprate);
		
		for(Map.Entry<String, Integer> entry : dropCategories.entrySet()){
			config.set("dropevent.categories." + entry.getKey(),entry.getValue());
		}
	}
	
	private void readLootTable() {
		List<Location> spawns = new ArrayList<Location>();
		FileConfiguration config = plugin.fileutil.getCustomConfig(lootTable);

		name = config.getString("dropevent.name");

		world = plugin.getServer()
				.getWorld(config.getString("dropevent.world"));
		duration = config.getInt("dropevent.duration");
		droprate = config.getInt("dropevent.droprate");

		Set<String> childNodes = config.getConfigurationSection(
				"dropevent.categories").getKeys(false);
		for (String child : childNodes) {
			dropCategories.put(child,
					config.getInt("dropevent.categories." + child + ".limit"));
		}
		childNodes = config.getConfigurationSection("dropevent.spawns")
				.getKeys(false);
		for (String child : childNodes) {
			String path = new StringBuilder("dropevent.spawns.").append(child)
					.toString();
			dropLocations.put(child,new Location(world, (double) config.getInt(path
					+ ".x"), (double) config.getInt(path + ".y"),
					(double) config.getInt(path + ".z")));
		}

		childNodes = config.getConfigurationSection("dropevent.drops").getKeys(
				false);
		for (String child : childNodes) {
			spawns.clear();
			String path = new StringBuilder("dropevent.drops.").append(child)
					.toString();
			for (String spawn : config.getStringList(path + ".spawnlist")) {
				
				spawns.add(dropLocations.get(spawn));
			}

			dropList.add(new Drop(config.getInt(path + ".id"), config
					.getInt(path + ".value"), config.getInt(path + ".amount"),
					config.getDouble(path + ".chance"), config.getInt(path
							+ ".limit"), config.getString(path + ".category"),
					spawns,name,plugin));
		}
	}
}
