package de.blockschmiede.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class FileUtil {
	
	private JavaPlugin plugin;
	
	private FileConfiguration customConfig = null;
	private File customConfigFile = null;
	private BufferedWriter logFile = null;
	
	
	public FileUtil(JavaPlugin instance){
		this.plugin=instance;
	}
	
	public boolean eventExists(String name){
		File temp = new File(plugin.getDataFolder()+File.separator+"events", name+".yml");
		System.out.println(temp);
		return temp.exists();
		
	}

 	public void reloadCustomConfig(String filename) {
		if (customConfigFile == null) {
			customConfigFile = new File(plugin.getDataFolder()+File.separator+"events", filename+".yml");
		}
		customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

		// Look for defaults in the jar
		InputStream defConfigStream = plugin.getResource(filename);
		if (defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration
					.loadConfiguration(defConfigStream);
			customConfig.setDefaults(defConfig);
		}
	}

	public FileConfiguration getCustomConfig(String filename) {
		if (customConfig == null) {
			reloadCustomConfig(filename);
		}
		return customConfig;
	}

	public void saveCustomConfig() {
		if (customConfig == null || customConfigFile == null) {
			return;
		}
		try {
			customConfig.save(customConfigFile);
		} catch (IOException ex) {
			Logger.getLogger(JavaPlugin.class.getName()).log(Level.SEVERE,
					"Could not save config to " + customConfigFile, ex);
		}
	}

	public void log(String eventName, String logtext){
		try {
		if(logFile==null){
			File loggerFile = new File(plugin.getDataFolder(), "dropzone.log");
			logFile = new BufferedWriter(new FileWriter(loggerFile),1024);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String uhrzeit = sdf.format(new Date());
		System.out.println(new StringBuilder("[").append(uhrzeit).append("] ").append(eventName).append(": ").append(logtext).toString());
		logFile.write(new StringBuilder("[").append(uhrzeit).append("] ").append(eventName).append(": ").append(logtext).toString());
		logFile.newLine();
		logFile.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
